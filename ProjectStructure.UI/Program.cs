﻿using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace ProjectStructure.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var configuration = new ConfigurationBuilder()
                    .AddJsonFile(@$"appsettings.json")
                    .Build();
                string baseAddress = configuration["BaseAddress"];

                using Menu menu = new Menu();
                var task1 = menu.StartAsync(baseAddress);
                Task.WaitAll(task1);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }

        }

    }
}

