﻿using Newtonsoft.Json;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ProjectStructure.UI
{
    public class QueriesService
    {
        private HttpClient _client;
        public QueriesService(HttpClient client)
        {
            _client = client;
        }

        // https://andrey.moveax.ru/post/csharp-sync-to-async
        public async Task<int> MarkRandomTaskWithDelay(int delay = 1000)
        {
            await Task.Delay(delay);

            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            await Task.Run(async () =>
            {
                try
                {
                    var result = await this.MarkRandomTask();
                    tcs.SetResult(result);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            });

            return await tcs.Task;
        }

        public async Task<int> MarkRandomTaskWithDelay2(int delay = 1000)
        {
            await Task.Delay(1000);

            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            Timer timer = new Timer() { AutoReset = true, Interval = delay };
            timer.Elapsed += async (sender, e) =>
            {
                try
                {
                    var taskId = await MarkRandomTask();
                    Console.WriteLine($"{taskId}");
                    //tcs.SetResult(taskId);
                }
                catch (Exception exc)
                {
                    tcs.SetException(exc);
                }
            };

            timer.Start();
            return await tcs.Task;
        }


        private async Task<int> MarkRandomTask()
        {
            // завантажувати список завдань
            var httpResponse = await _client.GetAsync(@$"/api/Tasks");
            if (!httpResponse.IsSuccessStatusCode)
            {
                throw new Exception(httpResponse.StatusCode.ToString());
            }
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var tasksAll = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);

            // вибирати випадковим чином одну з задач
            var tasks = tasksAll.Where(t => t.State != 2).ToList(); // невыполненные задания
            int count = tasks.Count; // кол-во невыполненных задач
            //Console.WriteLine($"Count: {count}");

            if (count == 0)
            {
                throw new Exception("No Task!");
            }

            Random rnd = new Random();
            int idRandomTask = rnd.Next(count);

            TaskDTO task = tasks[idRandomTask];
            //Console.WriteLine($"{task.Id} {task.Name} {task.State}");
            
            task.State = 2;

            // відправляти запит на сервер - позначити завдання як виконане

            var jsonInString = JsonConvert.SerializeObject(task);
            var stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            httpResponse = await _client.PutAsync(@"/api/Tasks", stringContent);

            if (httpResponse.IsSuccessStatusCode)
                return task.Id;
            else
                throw new Exception(httpResponse.StatusCode.ToString());

        }

    }
}
