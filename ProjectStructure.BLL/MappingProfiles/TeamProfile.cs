﻿using AutoMapper;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile() 
        {
            CreateMap<Team, TeamDTO>()
            .ReverseMap()
            ;
        }
    }
}
