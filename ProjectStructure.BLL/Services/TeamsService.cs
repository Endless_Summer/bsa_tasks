﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TeamsService : ITeamsService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public TeamsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateTeamAsync(TeamDTO item)
        {
            await db.Teams.CreateAsync(_mapper.Map<TeamDTO, Team>(item));
            await db.SaveAsync();
        }

        public async Task<TeamDTO> FindTeamByIdAsync(int id)
        {
            var entity = await db.Teams.FindByIdAsync(id);
            return _mapper.Map<Team, TeamDTO>(entity);
        }

        public async Task<IEnumerable<TeamDTO>> GetTeamsAsync()
        {
            var entities = await db.Teams.GetAsync();

            return _mapper.Map<IEnumerable<Team>, List<TeamDTO>>(entities);
        }

        public async Task RemoveTeamAsync(int id)
        {
            var team = await db.Teams.FindByIdAsync(id) ?? throw new ArgumentException($"Id {id} not found");
            db.Teams.Remove(team);
            await db.SaveAsync();
        }
        public async Task UpdateTeamAsync(TeamDTO item)
        {
            var team = _mapper.Map<TeamDTO, Team>(item);

            db.Teams.Update(team);
            await db.SaveAsync();
        }
    }
}
