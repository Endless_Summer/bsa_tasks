﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public UsersService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            db = unitOfWork;
        }

        public async Task CreateUserAsync(UserDTO item)
        {
            var user = _mapper.Map<UserDTO, User>(item);
            if (item.TeamId != null)
                user.Team = await db.Teams.FindByIdAsync((int)item.TeamId) ?? throw new ArgumentException($"Team with Id {item.TeamId} doesn't exist!");
            await db.Users.CreateAsync(user);

            await db.SaveAsync();
        }

        public async Task<UserDTO> FindUserByIdAsync(int id)
        {
            var entity = await db.Users.FindByIdAsync(id);
            return _mapper.Map<User, UserDTO>(entity);
        }


        public async Task<IEnumerable<UserDTO>> GetUsersAsync(bool asNoTracking = false)
        {
            var entities = await db.Users.GetAsync(asNoTracking);
            return _mapper.Map<IEnumerable<User>, List<UserDTO>>(entities);
        }

        public async Task RemoveUserAsync(int id)
        {
            var user = await db.Users.FindByIdAsync(id) ?? throw new ArgumentNullException($"Id {id} not found");

            db.Users.Remove(user);
            await db.SaveAsync();
        }

        public async Task UpdateUserAsync(UserDTO item)
        {
            var user = _mapper.Map<UserDTO, User>(item);
            db.Users.Update(user);
            await db.SaveAsync();
        }
    }
}
