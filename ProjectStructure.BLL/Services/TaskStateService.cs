﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TaskStateService : ITaskStateService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public TaskStateService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }
        public async Task CreateTaskStateAsync(TaskStateDTO item)
        {
            await db.TaskStates.CreateAsync(_mapper.Map<TaskStateDTO, TaskState>(item));
            await db.SaveAsync();
        }


        public async Task<TaskStateDTO> FindTaskStateByIdAsync(int id)
        {
            var entity = await db.TaskStates.FindByIdAsync(id);
            return _mapper.Map<TaskState, TaskStateDTO>(entity);
        }

        public async Task<IEnumerable<TaskStateDTO>> GetTaskStatesAsync()
        {
            var entities = await db.TaskStates.GetAsync();

            return _mapper.Map<IEnumerable<TaskState>, List<TaskStateDTO>>(entities);
        }

        public async Task RemoveTaskStateAsync(int id)
        {
            var taskState = await db.TaskStates.FindByIdAsync(id);

            if (taskState == null)
                throw new ArgumentNullException($"Id {id} not found");

            db.TaskStates.Remove(taskState);
            await db.SaveAsync();
        }

        public async Task UpdateTaskStateAsync(TaskStateDTO item)
        {
            var taskState = _mapper.Map<TaskStateDTO, TaskState>(item);

            db.TaskStates.Update(taskState);
            await db.SaveAsync();
        }
    }
}
