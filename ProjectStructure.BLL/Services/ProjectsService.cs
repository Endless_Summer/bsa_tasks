﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class ProjectsService : IProjectsService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public ProjectsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }
        public async Task CreateProjectAsync(ProjectDTO item)
        {
            var project = _mapper.Map<ProjectDTO, Project>(item);

            if (item.TeamId != null)
                project.Team = await db.Teams.FindByIdAsync((int)item.TeamId) ?? throw new ArgumentException($"Team with Id {item.TeamId} doesn't exist!");
            if (item.AuthorId != null)
                project.Author = await db.Users.FindByIdAsync((int)item.AuthorId) ?? throw new ArgumentException($"User with Id {item.AuthorId} doesn't exist!");

            await db.Projects.CreateAsync(project);
            await db.SaveAsync();
        }


        public async Task<ProjectDTO> FindProjectByIdAsync(int id)
        {
            var entity = await db.Projects.FindByIdAsync(id);
            return _mapper.Map<Project, ProjectDTO>(entity);
        }

        public async Task<IEnumerable<ProjectDTO>> GetProjectsAsync()
        {
            var entities = await db.Projects.GetAsync();
            return _mapper.Map<IEnumerable<Project>, List<ProjectDTO>>(entities);
        }

        public async Task RemoveProjectAsync(int id)
        {
            var project = await db.Projects.FindByIdAsync(id);

            if (project == null)
                throw new ArgumentException($"Id {id} not found");

            db.Projects.Remove(project);
            await db.SaveAsync();
        }

        public async Task UpdateProjectAsync(ProjectDTO item)
        {
            var project = _mapper.Map<ProjectDTO, Project>(item);

            db.Projects.Update(project);
            await db.SaveAsync();
        }
    }
}
