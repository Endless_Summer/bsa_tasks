﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TasksService : ITasksService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;
        public TasksService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateTaskAsync(TaskDTO item)
        {
            var task = _mapper.Map<TaskDTO, TaskEntity>(item);

            if (item.ProjectId != null)
                task.Project = await db.Projects.FindByIdAsync((int)item.ProjectId) ?? throw new ArgumentException($"Project with Id {item.ProjectId} doesn't exist!");
            if (item.PerformerId != null)
                task.Performer = await db.Users.FindByIdAsync((int)item.PerformerId) ?? throw new ArgumentException($"User with Id {item.PerformerId} doesn't exist!");

            task.TaskState = await db.TaskStates.FindByIdAsync((int)item.State) ?? throw new ArgumentException($"Task with Id {item.State} doesn't exist!");

            await db.Tasks.CreateAsync(task);
            await db.SaveAsync();
        }

        public async Task<TaskDTO> FindTaskByIdAsync(int id)
        {
            var entity = await db.Tasks.FindByIdAsync(id);
            return _mapper.Map<TaskEntity, TaskDTO>(entity);
        }
        public async Task<IEnumerable<TaskDTO>> GetTasksAsync(bool asNoTracking = false)
        {
            var entities = await db.Tasks.GetAsync(asNoTracking);
            return _mapper.Map<IEnumerable<TaskEntity>, List<TaskDTO>>(entities);
        }

        public async Task RemoveTaskAsync(int id)
        {
            var task = await db.Tasks.FindByIdAsync(id);

            if (task == null)
                throw new ArgumentNullException($"Id {id} not found");

            db.Tasks.Remove(task);
            await db.SaveAsync();
        }
        public async Task UpdateTaskAsync(TaskDTO item)
        {
            var task = _mapper.Map<TaskDTO, TaskEntity>(item);

            db.Tasks.Update(task);
            await db.SaveAsync();
        }

        public async Task<IEnumerable<TaskDTO>> GetUnfinishedTasksAsync(int userId)
        {
            if (await db.Users.FindByIdAsync(userId) == null)
                throw new ArgumentException($"User with Id {userId} doesn't exist!");

            return _mapper.Map<IEnumerable<TaskDTO>>(await db.Tasks.GetAsync(t => t.PerformerId == userId && t.State != 2));
        }
    }
}
