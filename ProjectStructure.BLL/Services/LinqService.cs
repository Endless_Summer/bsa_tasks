﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class LinqService : ILinqService
    {
        private readonly IUnitOfWork db;
        readonly IMapper _mapper;


        public LinqService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            db = unitOfWork;
            _mapper = mapper;
        }


        public async Task<IEnumerable<ProjectUserTasksCountDTO>> GetProjectUserTasksCountAsync(int userId) // Task 1
        {
            //var taskTasks = db.Tasks.GetAsync();
            //var taskProjects = db.Projects.GetAsync(p => p.AuthorId == userId);
            //await Task.WhenAll(taskTasks, taskProjects);
            //var tasks = await taskTasks;
            //var projects = await taskProjects;

            var tasks = await db.Tasks.GetAsync();
            var projects = await db.Projects.GetAsync(p => p.AuthorId == userId);

            var result = projects
                .GroupJoin(tasks, p => p.Id, t => t.ProjectId, (p, t) => new ProjectUserTasksCountDTO
                {
                    Project = _mapper.Map<ProjectDTO>(p),
                    UserTasksCount = t.Count()
                });

            return result;
        }

        public async Task<IEnumerable<TaskDTO>> GetUserTasksAsync(int userId) // Task 2
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(await db.Tasks.GetAsync(t => t.PerformerId == userId && t.Name.Length < 45));
        }

        public async Task<IEnumerable<TaskShortDTO>> GetFinishedTasksForUserAsync(int userId) // Task 3
        {
            var result = await db.Tasks.GetAsync(t => t.PerformerId == userId && t.FinishedAt.Year == DateTime.Now.Year && t.State == 2)
                .ContinueWith(t => t.Result
                .Select(t => new TaskShortDTO
                {
                    Id = t.Id,
                    Name = t.Name
                }));

            return result;
        }

        public async Task<IEnumerable<TeamUsersDTO>> GetAgeLimitTeamsAsync() // Task 4
        {
            //var taskUsers = db.Users.GetAsync();
            //var taskTeams = db.Teams.GetAsync();
            //await Task.WhenAll(taskUsers, taskTeams);
            //var users = await taskUsers;
            //var teams = await taskTeams;

            var users = await db.Users.GetAsync();
            var teams = await db.Teams.GetAsync();

            var result = users
                .Where(u => u.Birthday.Year < DateTime.Now.Year - 10)
                .Join(teams, u => u.Team?.Id, t => t.Id, (u, t) => new { User = u, Team = t })
                .GroupBy(x => x.Team.Id)
                .Select(x => new TeamUsersDTO
                {
                    Id = x.First().Team.Id,
                    Name = x.First().Team.Name,
                    Users = _mapper.Map<List<UserDTO>>(x.Select(y => y.User).OrderByDescending(z => z.RegisteredAt).ToList())
                });

            return result;
        }

        public async Task<IEnumerable<UserTasksDTO>> GetSortedUsersAsync() // TASK 5
        {
            //var taskUsers = db.Users.GetAsync();
            //var taskTasks = db.Tasks.GetAsync();
            //await Task.WhenAll(taskUsers, taskTasks);
            //var users = await taskUsers;
            //var task = await taskTasks;

            var users = await db.Users.GetAsync();
            var task = await db.Tasks.GetAsync();

            var result = users
                .GroupJoin(task, u => u.Id, t => t.PerformerId, (u, t) => new UserTasksDTO
                {
                    User = _mapper.Map<UserDTO>(u),
                    Tasks = _mapper.Map<List<TaskDTO>>(t.OrderByDescending(t => t.Name.Length).ToList())
                })
               .OrderBy(x => x.User.FirstName);

            return result;
        }


        public async Task<UserInfoDTO> GetUserLastProjectInfoAsync(int userId) // Task 6
        {
            //var taskUsers = db.Users.GetAsync(u => u.Id == userId);
            //var taskProjects = db.Projects.GetAsync();
            //await Task.WhenAll(taskUsers, taskProjects);
            //var users = await taskUsers;
            //var projects = await taskProjects;

            var users = await db.Users.GetAsync(u => u.Id == userId);
            var projects = await db.Projects.GetAsync();

            var result = users
                .GroupJoin(projects, u => u.Id, p => p.AuthorId, (u, p) => new { User = u, LastProject = p.OrderBy(p => p.CreatedAt).LastOrDefault() })
                .Select(x => new UserInfoDTO
                {
                    User = _mapper.Map<UserDTO>(x.User),
                    LastProject = _mapper.Map<ProjectDTO>(x.LastProject),
                    TasksLastProject = db.Tasks.Get(t => t.ProjectId == x.LastProject?.Id).Count(),
                    NotComletedTasks = db.Tasks.Get(t => t.PerformerId == x.User.Id && t.State != 2).Count(),
                    MaxTask = _mapper.Map<TaskDTO>(db.Tasks.Get(t => t.PerformerId == x.User.Id).OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault())
                }).FirstOrDefault();

            return result;
        }

        public async Task<IEnumerable<ProjectShortInfo>> GetProjectShortInfoAsync() // Task 7
        {
            //var taskUsers = db.Users.GetAsync();
            //var taskProjects = db.Projects.GetAsync();
            //var taskTasks = db.Tasks.GetAsync();
            //await Task.WhenAll(taskUsers, taskProjects, taskTasks);
            //var tasks = await taskTasks;
            //var users = await taskUsers;
            //var projects = await taskProjects;

            var tasks = await db.Tasks.GetAsync();
            var users = await db.Users.GetAsync();
            var projects = await db.Projects.GetAsync();

            var result = projects
                .GroupJoin(tasks, p => p.Id, t => t.ProjectId, (p, t) => new { Project = p, Tasks = t })
                .Select(x => new ProjectShortInfo
                {
                    Project = _mapper.Map<ProjectDTO>(x.Project),
                    LongDescriptionTask = _mapper.Map<TaskDTO>(x.Tasks.OrderBy(t => t.Description.Length).LastOrDefault()),
                    ShortestNameTask = _mapper.Map<TaskDTO>(x.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                    UserCount = (x.Project.Description.Length > 20 || x.Tasks.Count() < 3) ? users.Count(u => u.Team?.Id == x.Project.TeamId) : 0
                });

            return result;
        }
    }
}
