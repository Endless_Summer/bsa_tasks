﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITasksService
    {
        Task<IEnumerable<TaskDTO>> GetTasksAsync(bool asNoTracking = false);
        Task<TaskDTO> FindTaskByIdAsync(int id);
        Task CreateTaskAsync(TaskDTO item);
        Task RemoveTaskAsync(int id);
        Task UpdateTaskAsync(TaskDTO item);
        Task<IEnumerable<TaskDTO>> GetUnfinishedTasksAsync(int userId);

    }
}
