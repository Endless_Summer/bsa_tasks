﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IUsersService
    {
        Task<IEnumerable<UserDTO>> GetUsersAsync(bool asNoTracking = false);
        Task<UserDTO> FindUserByIdAsync(int id);
        Task CreateUserAsync(UserDTO item);
        Task RemoveUserAsync(int id);
        Task UpdateUserAsync(UserDTO item);
    }
}
