﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IProjectsService
    {
        Task<IEnumerable<ProjectDTO>> GetProjectsAsync();
        Task<ProjectDTO> FindProjectByIdAsync(int id);
        Task CreateProjectAsync(ProjectDTO item);
        Task RemoveProjectAsync(int id);
        Task UpdateProjectAsync(ProjectDTO item);
    }
}
