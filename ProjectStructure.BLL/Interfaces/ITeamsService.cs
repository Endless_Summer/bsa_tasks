﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITeamsService
    {
        Task<IEnumerable<TeamDTO>> GetTeamsAsync();
        Task<TeamDTO> FindTeamByIdAsync(int id);
        Task CreateTeamAsync(TeamDTO item);
        Task RemoveTeamAsync(int id);
        Task UpdateTeamAsync(TeamDTO item);
    }
}
