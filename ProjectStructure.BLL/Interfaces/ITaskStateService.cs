﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITaskStateService
    {
        Task<IEnumerable<TaskStateDTO>> GetTaskStatesAsync();
        Task<TaskStateDTO> FindTaskStateByIdAsync(int id);
        Task CreateTaskStateAsync(TaskStateDTO item);
        Task RemoveTaskStateAsync(int id);
        Task UpdateTaskStateAsync(TaskStateDTO item);
    }
}
