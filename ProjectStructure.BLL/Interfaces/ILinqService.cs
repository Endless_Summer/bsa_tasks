﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ILinqService
    {
        Task<IEnumerable<ProjectUserTasksCountDTO>> GetProjectUserTasksCountAsync(int userId); // Task 1
        Task<IEnumerable<TaskDTO>> GetUserTasksAsync(int userId); // Task 2
        Task<IEnumerable<TaskShortDTO>> GetFinishedTasksForUserAsync(int userId); // Task 3
        Task<IEnumerable<TeamUsersDTO>> GetAgeLimitTeamsAsync(); // TASK 4
        Task<IEnumerable<UserTasksDTO>> GetSortedUsersAsync(); // TASK 5
        Task<UserInfoDTO> GetUserLastProjectInfoAsync(int userId); // Task 6
        Task<IEnumerable<ProjectShortInfo>> GetProjectShortInfoAsync(); // Task 7

    }
}
