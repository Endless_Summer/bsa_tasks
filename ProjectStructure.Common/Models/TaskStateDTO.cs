﻿
namespace ProjectStructure.Common.Models
{
    public class TaskStateDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
