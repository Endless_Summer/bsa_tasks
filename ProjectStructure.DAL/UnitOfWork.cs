﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;
using System.Threading.Tasks;

namespace ProjectStructure.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        DbContext context;
        IGenericRepository<User> userRepository;
        IGenericRepository<Team> teamRepository;
        IGenericRepository<TaskState> taskStateRepository;
        IGenericRepository<TaskEntity> taskRepository;
        IGenericRepository<Project> projectRepository;

        public UnitOfWork(DbContext context)
        {
            this.context = context;
        }

        public IGenericRepository<User> Users
        {
            get
            {
                if (userRepository == null)
                    userRepository = new GenericRepository<User>(context);
                return userRepository;
            }
        }

        public IGenericRepository<Team> Teams
        {
            get
            {
                if (teamRepository == null)
                    teamRepository = new GenericRepository<Team>(context);
                return teamRepository;
            }
        }

        public IGenericRepository<TaskState> TaskStates
        {
            get
            {
                if (taskStateRepository == null)
                    taskStateRepository = new GenericRepository<TaskState>(context);
                return taskStateRepository;
            }
        }

        public IGenericRepository<TaskEntity> Tasks
        {
            get
            {
                if (taskRepository == null)
                    taskRepository = new GenericRepository<TaskEntity>(context);
                return taskRepository;
            }
        }

        public IGenericRepository<Project> Projects
        {
            get
            {
                if (projectRepository == null)
                    projectRepository = new GenericRepository<Project>(context);
                return projectRepository;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public void Save()
        {
            context.SaveChanges();
        }
        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

    }
}
