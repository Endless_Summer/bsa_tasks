﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAsync(bool asNoTracking = false);
        Task<TEntity> FindByIdAsync(int id);
        Task CreateAsync(TEntity item);
        void Remove(TEntity item);
        void Update(TEntity item);
        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);
        Task<IEnumerable<TEntity>> GetAsync(Func<TEntity, bool> predicate);

    }


}
