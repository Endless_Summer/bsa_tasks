﻿using Microsoft.Extensions.Caching.Distributed;
using ProjectStructure.DAL.Entities;
using System;
using System.Threading.Tasks;

namespace ProjectStructure.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<User> Users { get; }
        IGenericRepository<Team> Teams { get; }
        IGenericRepository<TaskState> TaskStates { get; }
        IGenericRepository<TaskEntity> Tasks { get; }
        IGenericRepository<Project> Projects { get; }

        void Save();
        Task SaveAsync();

    }
}
