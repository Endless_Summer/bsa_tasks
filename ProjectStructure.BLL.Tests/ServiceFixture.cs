﻿using AutoMapper;
using ProjectStructure.BLL.MappingProfiles;
using System;

namespace ProjectStructure.BLL.Tests
{
    public class ServiceFixture : IDisposable //where T : class
    {
        public ServiceFixture()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TaskStateProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });

            GetMapper = config.CreateMapper();

        }

        public IMapper GetMapper { get; }

        public void Dispose()
        {

        }
    }
}
