﻿using AutoMapper;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
    public class OtherServicesTests : IClassFixture<ServiceFixture>, IDisposable
    {
        private readonly IUsersService _usersService;
        private readonly ITasksService _tasksService;
        private readonly ProjectStructureDbContext context;
        private readonly IUnitOfWork unitOfWork;

        readonly IUnitOfWork _fakeUnitOfWork;
        readonly IMapper _imapper;

        public OtherServicesTests(ServiceFixture fixture)
        {
            DbContextOptions<ProjectStructureDbContext> options = new DbContextOptionsBuilder<ProjectStructureDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDB", new InMemoryDatabaseRoot())
                .Options;

            context = new ProjectStructureDbContext(options);

            context.Database.EnsureCreated();

            unitOfWork = new UnitOfWork(context);

            _usersService = new UsersService(unitOfWork, fixture.GetMapper);
            _tasksService = new TasksService(unitOfWork, fixture.GetMapper);


            _fakeUnitOfWork = A.Fake<IUnitOfWork>();
            _imapper = A.Fake<IMapper>();

        }
        public void Dispose()
        {
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async Task CreateUser_WhenRightDTO_ThenAddUserToDBAsync()
        {
            var countUsersBeforeCreate = await _usersService.GetUsersAsync()
                .ContinueWith(t => t.Result.Count());

            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 2,
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            await _usersService.CreateUserAsync(userDTO);

            var countUsersAfterCreate = await _usersService.GetUsersAsync()
                .ContinueWith(t => t.Result.Count());
            var userTest = await _usersService.GetUsersAsync()
                .ContinueWith(t => t.Result.FirstOrDefault(u => u.FirstName == "FirstName" && u.LastName == "LastName"));
                

            Assert.True(countUsersAfterCreate - countUsersBeforeCreate == 1);

            Assert.NotNull(userTest);
            Assert.Equal("FirstName", userTest.FirstName);
            Assert.Equal("LastName", userTest.LastName);
            Assert.Equal("email@mail.com", userTest.Email);
            Assert.Equal(2, userTest.TeamId);
        }

        [Fact]
        public async Task CreateUser_WhenCallServiceMethod_ThenCallDalMethodAsync()
        {
            UsersService usersService = new UsersService(_fakeUnitOfWork, _imapper);

            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 2,
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            await usersService.CreateUserAsync(userDTO);

            A.CallTo(() => _fakeUnitOfWork.Users.CreateAsync(A<User>._)).MustHaveHappenedOnceExactly();
        }



        [Fact]
        public async Task CreateUser_WhenFalseTeamIdDTO_ThenThrowArgumentExceptionAsync()
        {
            var countUsersBeforeCreate = await _usersService.GetUsersAsync()
                .ContinueWith(t => t.Result.Count());

            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 99999,
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            var countUsersAfterCreate = await _usersService.GetUsersAsync()
                .ContinueWith(t => t.Result.Count());

            await Assert.ThrowsAsync<ArgumentException>(() => _usersService.CreateUserAsync(userDTO));
            Assert.True(countUsersAfterCreate - countUsersBeforeCreate == 0);
        }

        [Fact]
        public async Task ChangeTaskToComplete_WhenTrueDTO_ThenChangedAsync()
        {
            var taskStateCreated = await _tasksService.GetTasksAsync(true)
                .ContinueWith(t => t.Result.FirstOrDefault(t => t.State == 1));

            int idTaskState = taskStateCreated.Id;

            taskStateCreated.State = 2;
            var newTask = new TaskDTO()
            {
                Id = taskStateCreated.Id,
                Name = taskStateCreated.Name,
                Description = taskStateCreated.Description,
                CreatedAt = taskStateCreated.CreatedAt,
                FinishedAt = taskStateCreated.FinishedAt,
                PerformerId = taskStateCreated.PerformerId,
                ProjectId = taskStateCreated.ProjectId,
                State = taskStateCreated.State
            };

            await _tasksService.UpdateTaskAsync(newTask);

            var taskStateChanged = await _tasksService.FindTaskByIdAsync(idTaskState);

            Assert.Equal(2, taskStateChanged.State);
        }

        [Fact]
        public async Task ChangeTaskToComplete_WhenCallServiceMethod_ThenCallDalMethodAsync()
        {
            TasksService tasksService = new TasksService(_fakeUnitOfWork, _imapper);

            var task = new TaskDTO()
            {
                Id = 2,
                Name = "Name",
                Description = "Description",
                CreatedAt = DateTime.Now,
                FinishedAt = DateTime.Now,
                PerformerId = 1,
                ProjectId = 1,
                State = 2
            };

            await tasksService.UpdateTaskAsync(task);

            A.CallTo(() => _fakeUnitOfWork.Tasks.Update(A<TaskEntity>._)).MustHaveHappenedOnceExactly();
        }


        [Fact]
        public async Task AddUserToTeam_WhenTrueDTO_ThenAddedAsync()
        {
            var user = await _usersService.GetUsersAsync(true)
                .ContinueWith(t => t.Result.FirstOrDefault(u => u.TeamId == null));

            int indexUser = user.Id;

            user.TeamId = 2;

            await _usersService.UpdateUserAsync(user);

            var userChanged = await _usersService.FindUserByIdAsync(indexUser);

            Assert.Equal(2, userChanged.TeamId);

        }

        [Fact]
        public async Task AddUserToTeam_WhenCallServiceMethod_ThenCallDalMethodAsync()
        {
            UsersService usersService = new UsersService(_fakeUnitOfWork, _imapper);

            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 2,
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            await usersService.UpdateUserAsync(userDTO);

            A.CallTo(() => _fakeUnitOfWork.Users.Update(A<User>._)).MustHaveHappenedOnceExactly();

        }


        [Theory]
        [InlineData(1, new int[] { 117, 177 })]
        [InlineData(2, new int[] { 2, 37, 76, 181 })]
        [InlineData(3, new int[] { 57, 64, 86, 115, 130, 138, 167, 194 })]
        [InlineData(4, new int[] { 87, 129, 171, 173 })]
        [InlineData(5, new int[] { 155 })]

        public async Task GetUnfinishedTasks_WhenTrueId_ThenGetArray(int userId, int[] expectedArray)
        {
            // result = await _tasksService.GetUnfinishedTasksAsync(id).ContinueWith(t => t.Result.ToList());
            // List<TaskDTO> result = query.ToList();

            List<TaskDTO> result = await _tasksService.GetUnfinishedTasksAsync(userId).ContinueWith(t => t.Result.ToList());

            int[] actualArray = result.Select(t => t.Id).ToArray();

            Array.Sort(actualArray);
            Array.Sort(expectedArray);

            // Assert

            Assert.True(actualArray.SequenceEqual(expectedArray));
        }


        [Theory]
        [InlineData(9999)]
        [InlineData(-1)]
        [InlineData(0)]

        public async Task GetUnfinishedTasks_WhenFalseId_ThenThrowArgumentException(int userId)
        {
            await Assert.ThrowsAsync<ArgumentException>(() => _tasksService.GetUnfinishedTasksAsync(userId));
        }


    }
}
