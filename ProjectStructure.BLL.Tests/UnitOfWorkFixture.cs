﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.DAL;
using System;

namespace ProjectStructure.BLL.Tests
{
    public class UnitOfWorkFixture : IDisposable
    {
        readonly DbContextOptions<ProjectStructureDbContext> options;
        readonly IUnitOfWork unitOfWork;
        public MapperConfiguration config;
        IMapper mapper;
        public UnitOfWorkFixture()
        {
            options = new DbContextOptionsBuilder<ProjectStructureDbContext>()
                .UseInMemoryDatabase(databaseName: "Test")
                .Options;

            config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TaskStateProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });

            unitOfWork = new UnitOfWork(new ProjectStructureDbContext(options));
        }
        public void Dispose()
        {
            // Method intentionally left empty.
        }

        public IUnitOfWork GetUnitOfWork
        {
            get
            {
                return unitOfWork;
            }
        }

        public IMapper GetMapper
        {
            get
            {
                if (mapper == null)
                    mapper = config.CreateMapper();
                return mapper;
            }
        }

    }
}
