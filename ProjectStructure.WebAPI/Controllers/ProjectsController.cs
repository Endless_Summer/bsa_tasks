﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;
        public ProjectsController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }
        // GET: api/Projects
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            return Ok(await _projectsService.GetProjectsAsync());
        }

        // GET api/Projects/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            ProjectDTO result = await _projectsService.FindProjectByIdAsync(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        // POST api/Projects
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ProjectDTO value)
        {
            try
            {
                await _projectsService.CreateProjectAsync(value);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        // PUT api/Projects/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] ProjectDTO value)
        {
            try
            {
                await _projectsService.UpdateProjectAsync(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/Projects/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _projectsService.RemoveProjectAsync(id);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return NoContent();
        }
    }
}
