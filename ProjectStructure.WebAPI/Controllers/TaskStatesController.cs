﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        readonly ITaskStateService _taskStateService;
        public TaskStatesController(ITaskStateService taskStateService)
        {
            _taskStateService = taskStateService;
        }

        // GET: api/TaskStates
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskStateDTO>>> Get()
        {
            return Ok(await _taskStateService.GetTaskStatesAsync());
        }

        // GET api/TaskStates/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskStateDTO>> Get(int id)
        {
            TaskStateDTO result = await _taskStateService.FindTaskStateByIdAsync(id);

            if (result == null)
                return NotFound(null);

            return Ok(result);
        }

        // POST api/TaskStates>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TaskStateDTO value)
        {
            if (value == null)
                return BadRequest(value);

            await _taskStateService.CreateTaskStateAsync(value);
            return Ok();
        }

        // PUT api/TaskStates/
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TaskStateDTO value)
        {
            try
            {
                await _taskStateService.UpdateTaskStateAsync(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/TaskStates/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _taskStateService.RemoveTaskStateAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }
    }
}
