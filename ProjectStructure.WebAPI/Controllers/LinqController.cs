﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        readonly ILinqService _linqService;
        public LinqController(ILinqService linqService)
        {
            _linqService = linqService;

        }

        // GET: api/Linq/ProjectUserTasksCount/1
        [HttpGet("ProjectUserTasksCount/{id}")] // Task 1
        public async Task<ActionResult<IEnumerable<ProjectUserTasksCountDTO>>> GetProjectUserTasksCount(int id)
        {
            return Ok(await _linqService.GetProjectUserTasksCountAsync(id));
        }

        // GET: api/Linq/UserTasks/1
        [HttpGet("UserTasks/{id}")] // Task 2
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUserTasks(int id)
        {
            return Ok(await _linqService.GetUserTasksAsync(id));
        }

        // GET: api/Linq/FinishedTasksForUser/1
        [HttpGet("FinishedTasksForUser/{id}")] // Task 3
        public async Task<ActionResult<IEnumerable<TaskShortDTO>>> GetFinishedTasksForUser(int id)
        {
            return Ok(await _linqService.GetFinishedTasksForUserAsync(id));
        }

        // GET: api/Linq/AgeLimitTeams
        [HttpGet("AgeLimitTeams")] // Task 4
        public async Task<ActionResult<IEnumerable<TeamUsersDTO>>> GetAgeLimitTeams()
        {
            return Ok(await _linqService.GetAgeLimitTeamsAsync());
        }

        // GET: api/Linq/SortedUsers
        [HttpGet("SortedUsers")] // Task 5
        public async Task<ActionResult<IEnumerable<UserTasksDTO>>> GetSortedUsers()
        {
            return Ok(await _linqService.GetSortedUsersAsync());
        }


        // GET: api/Linq/UserLastProjectInfo/1
        [HttpGet("UserLastProjectInfo/{id}")] // Task 6
        public async Task<ActionResult<UserInfoDTO>> GetUserLastProjectInfo(int id)
        {
            return Ok(await _linqService.GetUserLastProjectInfoAsync(id));
        }

        // GET: api/Linq/GetProjectShortInfo
        [HttpGet("ProjectShortInfo")] // Task 7
        public async Task<ActionResult<IEnumerable<ProjectShortInfo>>> GetProjectShortInfoAsync()
        {
            return Ok(await _linqService.GetProjectShortInfoAsync());
        }
    }
}
