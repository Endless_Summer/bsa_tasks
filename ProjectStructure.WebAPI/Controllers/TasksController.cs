﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        readonly ITasksService _tasksService;
        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        // GET: api/Tasks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> Get()
        {
            return Ok(await _tasksService.GetTasksAsync());
        }

        // GET api/Tasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> Get(int id)
        {
            TaskDTO result = await _tasksService.FindTaskByIdAsync(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        // POST api/Tasks
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TaskDTO value)
        {
            await _tasksService.CreateTaskAsync(value);
            return Ok();
        }

        // PUT api/Tasks/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TaskDTO value)
        {
            try
            {
                await _tasksService.UpdateTaskAsync(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/Tasks/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _tasksService.RemoveTaskAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // GET: api/Tasks/GetUnfinishedTasksForUser/1
        [HttpGet("UnfinishedTasksForUser/{id}")]
        public async Task<ActionResult<TaskDTO>> GetUnfinishedTasksForUser(int id)
        {
            List<TaskDTO> result;

            try
            {
                result = await _tasksService.GetUnfinishedTasksAsync(id).ContinueWith(t => t.Result.ToList());
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (AggregateException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            if (result.Count == 0)
                return NotFound();

            return Ok(result);
        }


    }
}
