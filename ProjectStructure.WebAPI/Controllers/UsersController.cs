﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _userService;

        public UsersController(IUsersService userService)
        {
            _userService = userService;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> Get()
        {
            var result = await _userService.GetUsersAsync();
            return Ok(result);
        }


        // GET api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            UserDTO result = await _userService.FindUserByIdAsync(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }


        // POST api/Users
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] UserDTO value)
        {
            try
            {
                await _userService.CreateUserAsync(value);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return Ok();
        }

        // PUT api/Users/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UserDTO value)
        {
            try
            {
                await _userService.UpdateUserAsync(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _userService.RemoveUserAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
            return NoContent();
        }
    }
}
