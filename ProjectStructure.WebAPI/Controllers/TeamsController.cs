﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService _teamsService;
        public TeamsController(ITeamsService teamsService)
        {
            _teamsService = teamsService;
        }
        // GET: api/Teams
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Get()
        {
            return Ok(await _teamsService.GetTeamsAsync());
        }

        // GET api/Teams/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            TeamDTO result = await _teamsService.FindTeamByIdAsync(id);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        // POST api/Teams
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TeamDTO value)
        {
            await _teamsService.CreateTeamAsync(value);
            return Ok();
        }

        // PUT api/Teams/5
        [HttpPut()]
        public async Task<ActionResult> Put([FromBody] TeamDTO value)
        {
            try
            {
                await _teamsService.UpdateTeamAsync(value);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/Teams/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _teamsService.RemoveTeamAsync(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
            return NoContent();
        }
    }
}
