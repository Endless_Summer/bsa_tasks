﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class ControllersIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        ProjectStructureDbContext db;
        public ControllersIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    var descriptor = services.SingleOrDefault(
                        d => d.ServiceType ==
                            typeof(DbContextOptions<ProjectStructureDbContext>));

                    if (descriptor != null)
                    {
                        services.Remove(descriptor);
                    }

                    services.AddDbContext<ProjectStructureDbContext>(options => options.UseInMemoryDatabase("TestDB")); // , new InMemoryDatabaseRoot()
                    // Build the service provider.
                    var sp = services.BuildServiceProvider();
                    // Create a scope to obtain a reference to the database
                    using (var scope = sp.CreateScope())
                    {
                        var scopedServices = scope.ServiceProvider;
                        db = scopedServices.GetRequiredService<ProjectStructureDbContext>();

                        // Ensure the database is created.
                        db.Database.EnsureDeleted();
                        db.Database.EnsureCreated();
                    }
                });
            }
            ).CreateClient();

        }
        public void Dispose()
        {
            //_client.Dispose();
        }


        [Fact]
        public async Task AddProject_WhenTrueIdDTO_ThenAddedAndCodeOKAsync()
        {
            // Кол-во записей перед добавлением
            var httpResponse = await _client.GetAsync(@$"api/Projects");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int counProjectBeforeAdd = entity.Count;

            // Add
            ProjectDTO projectDTO = new ProjectDTO()
            {
                Name = "TestProject",
                Description = "Description",
                AuthorId = 1,
                TeamId = 2,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(6)
            };

            string jsonInString = JsonConvert.SerializeObject(projectDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = await _client.PostAsync(@"api/Projects", stringContent);

            // Кол-во записей после добавления
            httpResponse = await _client.GetAsync(@$"api/Projects");
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int counProjectAfterAdd = entity.Count;

            // Поиск добавленной записи
            var project = entity.FirstOrDefault(p => p.Name == "TestProject");


            Assert.Equal(HttpStatusCode.OK, httpResponsePost.StatusCode);
            Assert.True(counProjectAfterAdd - counProjectBeforeAdd == 1);
            Assert.NotNull(project);

        }

        [Fact]
        public async Task AddProject_WhenFalsedDTO_ThenCodeBadRequestAsync()
        {
            // Кол-во записей перед добавлением
            var httpResponse = await _client.GetAsync(@$"api/Projects");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);

            // Add
            ProjectDTO projectDTO = new ProjectDTO()
            {
                Name = "TestProject",
                Description = "Description",
                AuthorId = 9999,
                TeamId = 2,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(6)
            };

            string jsonInString = JsonConvert.SerializeObject(projectDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = await _client.PostAsync(@"api/Projects", stringContent);

            Assert.Equal(HttpStatusCode.BadRequest, httpResponsePost.StatusCode);
        }

        [Fact]
        public async Task DeleteUser_WhenTrueIdDTO_ThenDeletedAndCodeNoContentAsync()
        {
            // Кол-во записей перед удалением
            var httpResponse = await _client.GetAsync(@$"api/Users");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = await _client.DeleteAsync(@"api/Users/4");

            // Кол-во записей после удаления
            httpResponse = await _client.GetAsync(@$"api/Users");
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            // Поиск удаленной записи
            var user = entity.FirstOrDefault(u => u.Id == 4);

            Assert.Equal(HttpStatusCode.NoContent, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete - countProjectAfterDelete == 1);
            Assert.Null(user);

        }


        [Fact]
        public async Task DeleteUser_WhenFaulseIdDTO_ThenCodeBadRequestAsync()
        {
            // Кол-во записей перед удалением
            var httpResponse = await _client.GetAsync(@$"api/Users");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = await _client.DeleteAsync(@"api/Users/9999");

            // Кол-во записей после удаления
            httpResponse = await _client.GetAsync(@$"api/Users");
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            Assert.Equal(HttpStatusCode.BadRequest, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete == countProjectAfterDelete);

        }

        [Fact]
        public async Task AddTeam_WhenTrueIdDTO_ThenAddedAndCodeOKAsync()
        {
            // Кол-во записей перед добавлением
            var httpResponse = await _client.GetAsync(@$"api/Teams");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<List<TeamDTO>>(stringResponse);
            int countTeamsBeforeAdd = entity.Count;

            // Add
            TeamDTO teamDTO = new TeamDTO()
            {
                Name = "TestTeam",
                CreatedAt = DateTime.Now
            };

            string jsonInString = JsonConvert.SerializeObject(teamDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = await _client.PostAsync(@"api/Teams", stringContent);

            // Кол-во записей после добавления
            httpResponse = await _client.GetAsync(@$"api/Teams");
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            entity = JsonConvert.DeserializeObject<List<TeamDTO>>(stringResponse);
            int countTeamsAfterAdd = entity.Count;

            // Поиск добавленной записи
            var team = entity.FirstOrDefault(p => p.Name == "TestTeam");


            Assert.Equal(HttpStatusCode.OK, httpResponsePost.StatusCode);
            Assert.True(countTeamsAfterAdd - countTeamsBeforeAdd == 1);
            Assert.NotNull(team);

        }


        [Fact]
        public async Task DeleteTask_WhenTrueIdDTO_ThenDeletedAndCodeNoContentAsync()
        {
            // Кол-во записей перед удалением
            var httpResponse = await _client.GetAsync(@$"api/Tasks");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = await _client.DeleteAsync(@"api/Tasks/2");

            // Кол-во записей после удаления
            httpResponse = await _client.GetAsync(@$"api/Tasks");
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            // Поиск удаленной записи
            var task = entity.FirstOrDefault(u => u.Id == 2);

            Assert.Equal(HttpStatusCode.NoContent, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete - countProjectAfterDelete == 1);
            Assert.Null(task);

        }


        [Fact]
        public async Task DeleteTask_WhenFaulseIdDTO_ThenCodeBadRequestAsync()
        {
            // Кол-во записей перед удалением
            var httpResponse = await _client.GetAsync(@$"api/Tasks");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = await _client.DeleteAsync(@"api/Tasks/9999");

            // Кол-во записей после удаления
            httpResponse = await _client.GetAsync(@$"api/Tasks");
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            Assert.Equal(HttpStatusCode.BadRequest, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete == countProjectAfterDelete);

        }

        [Fact]
        public async Task AddUser_WhenTrueIdDTO_ThenAddedAndCodeOKAsync()
        {
            // Кол-во записей перед добавлением
            var httpResponse = await _client.GetAsync(@$"api/Users");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<List<UserDTO>>(stringResponse);
            int countUsersBeforeAdd = entity.Count;

            // Add
            UserDTO userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                Birthday = DateTime.Now.AddYears(-20),
                RegisteredAt = DateTime.Now,
                TeamId = 2,
            };

            string jsonInString = JsonConvert.SerializeObject(userDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = await _client.PostAsync(@"api/Users", stringContent);

            // Кол-во записей после добавления
            httpResponse = await _client.GetAsync(@$"api/Users");
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            entity = JsonConvert.DeserializeObject<List<UserDTO>>(stringResponse);
            int countUsersAfterAdd = entity.Count;

            // Поиск добавленной записи
            var project = entity.FirstOrDefault(p => p.FirstName == "FirstName");


            Assert.Equal(HttpStatusCode.OK, httpResponsePost.StatusCode);
            Assert.True(countUsersAfterAdd - countUsersBeforeAdd == 1);
            Assert.NotNull(project);

        }

        [Fact]
        public async Task AddUser_WhenFalsedDTO_ThenCodeBadRequestAsync()
        {
            // Add
            UserDTO userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                Birthday = DateTime.Now.AddYears(-20),
                RegisteredAt = DateTime.Now,
                TeamId = 9999,
            };

            string jsonInString = JsonConvert.SerializeObject(userDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = await _client.PostAsync(@"api/Users", stringContent);

            Assert.Equal(HttpStatusCode.NotFound, httpResponsePost.StatusCode);
        }


        [Theory]
        [InlineData(1, new int[] { 117, 177 })]
        [InlineData(2, new int[] { 2, 37, 76, 181 })]
        [InlineData(3, new int[] { 57, 64, 86, 115, 130, 138, 167, 194 })]
        [InlineData(4, new int[] { 87, 129, 171, 173 })]
        [InlineData(5, new int[] { 155 })]

        public async Task GetUnfinishedTasks_WhenTrueId_ThenGetArrayAsync(int userId, int[] expectedArray)
        {
            // Кол-во записей перед удалением
            var httpResponse = _client.GetAsync(@$"api/Tasks/UnfinishedTasksForUser/{userId}").Result;
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);

            int[] actualArray = entity.Select(t => t.Id).ToArray();

            Array.Sort(actualArray);
            Array.Sort(expectedArray);

            // Assert

            Assert.True(actualArray.SequenceEqual(expectedArray));
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);

        }



        [Theory]
        [InlineData(9999)]
        [InlineData(-1)]
        [InlineData(0)]

        public async Task GetUnfinishedTasks_WhenFaulseId_ThenThrowArgumentExceptionAsync(int userId)
        {
            var httpResponse = await _client.GetAsync(@$"api/Tasks/UnfinishedTasksForUser/{userId}");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);

        }

    }
}
