﻿using Microsoft.AspNetCore.Mvc.Testing;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup> where TStartup : class
    {
        // https://blog.markvincze.com/overriding-configuration-in-asp-net-core-integration-tests/

        // https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-3.1


        //protected override void ConfigureWebHost(IWebHostBuilder builder)
        //{
        //    builder.ConfigureServices(services =>
        //    {
        //        // Remove the app's ApplicationDbContext registration.
        //        var descriptor = services.SingleOrDefault(
        //            d => d.ServiceType ==
        //                typeof(DbContextOptions<ProjectStructureDbContext>));

        //        if (descriptor != null)
        //        {
        //            services.Remove(descriptor);
        //        }

        //        // Add ApplicationDbContext using an in-memory database for testing.
        //        services.AddDbContext<ProjectStructureDbContext>(options =>
        //        {
        //            options.UseInMemoryDatabase("InMemoryDbForTesting");
        //        });

        //        // Build the service provider.
        //        var sp = services.BuildServiceProvider();

        //        // Create a scope to obtain a reference to the database
        //        // context(ApplicationDbContext).
        //        using (var scope = sp.CreateScope())
        //        {
        //            var scopedServices = scope.ServiceProvider;
        //            var db = scopedServices.GetRequiredService<ProjectStructureDbContext>();
        //            //var logger = scopedServices
        //            //    .GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

        //            // Ensure the database is created.
        //            //db.Database.EnsureCreated();

        //            //try
        //            //{
        //            //    // Seed the database with test data.
        //            //    Utilities.InitializeDbForTests(db);
        //            //}
        //            //catch (Exception ex)
        //            //{
        //            //    logger.LogError(ex, "An error occurred seeding the " +
        //            //        "database with test messages. Error: {Message}", ex.Message);
        //            //}
        //        }
        //    });
        //}


        //protected override void ConfigureWebHost(IWebHostBuilder builder)
        //{
        //    builder.ConfigureTestServices(services =>
        //    {
        //        // We can further customize our application setup here.
        //    });
        //}

        //public CustomWebApplicationFactory()
        //{
        //    this.WithWebHostBuilder(builder =>
        //    {
        //        builder.ConfigureServices(services =>
        //        {
        //            // ...
        //        });
        //    });
        //}
    }
}


